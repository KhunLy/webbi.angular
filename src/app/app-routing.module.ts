import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { Demo1Component } from './demo1/demo1.component';
import { Ex1Component } from './ex1/ex1.component';
import { Demo2Component } from './demo2/demo2.component';
import { PokedexComponent } from './pokedex/pokedex.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'demo1', component: Demo1Component },
  { path: 'ex1', component: Ex1Component },
  { path: 'meteo', component: Demo2Component },
  { path: 'pokedex', component: PokedexComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
