import { Component, OnInit, Input } from '@angular/core';
import { PokemonDetails } from 'src/app/models/pokemon-details';
import { HttpClient } from '@angular/common/http';
import { Label } from 'ng2-charts';
import { ChartDataSets, RadialChartOptions } from 'chart.js';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  model: PokemonDetails;
  labels: Label[] = [
    "Speed", "HP", "Defense", "Attack", "Defense Spec", "Attack Spec"
  ];
  datasets: ChartDataSets[] = [];

  options: RadialChartOptions = {
    scale: {
      ticks: {
        suggestedMin: 0,
        suggestedMax: 150
      }
    }
  }

  @Input() set pokemonUrl(url: string) {
    if(url != null) {
      console.log(2, url);
      // requete vers l'api pour récupérer mon model
      this.httpClient.get<PokemonDetails>(url)
        .subscribe(data => {
          this.model = data;
          this.datasets.push({
            label: data.name,
            data: [
              data.stats[0].base_stat,
              data.stats[5].base_stat,
              data.stats[3].base_stat,
              data.stats[4].base_stat,
              data.stats[1].base_stat,
              data.stats[2].base_stat,
            ]
          });
        });
    }
  }

  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
  }

}
