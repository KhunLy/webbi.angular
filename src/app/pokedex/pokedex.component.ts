import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Pokedex } from '../models/pokedex';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss']
})
export class PokedexComponent implements OnInit {

  basePokeUrl: string = "https://pokeapi.co/api/v2/pokemon";
  model: Pokedex;
  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
    // récuperer dans l'api mon model
    this.httpClient.get<Pokedex>(this.basePokeUrl)
      .subscribe(
        data => this.model = data,
        error => console.log(error)
      );
  }

  prev() {
    this.httpClient.get<Pokedex>(this.model.previous)
    .subscribe(
      data => this.model = data,
      error => console.log(error)
    );
  }

  next() {
    this.httpClient.get<Pokedex>(this.model.next)
    .subscribe(
      data => this.model = data,
      error => console.log(error)
    );
  }

  url: string;

  changePokemon(url: string) {
    console.log(1, url);
    this.url = url;
  }

}
