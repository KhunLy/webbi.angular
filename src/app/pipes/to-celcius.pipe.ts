import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toCelcius'
})
export class ToCelciusPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): number {
    return parseFloat((value - 273.15).toFixed(2));
  }

}
