import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'toUrl'
})
export class ToUrlPipe implements PipeTransform {

  baseImgUrl = "http://openweathermap.org/img/wn/{iconName}@2x.png";
  transform(value: string, ...args: any[]): string {
    return this.baseImgUrl
      .replace('{iconName}', value);
  }

}
