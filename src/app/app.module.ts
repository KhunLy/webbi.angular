import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbSidebarModule, NbButtonModule, NbInputModule, NbMenuModule, NbIconModule, NbCardModule, NbListModule, NbToastrModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { Demo1Component } from './demo1/demo1.component';
import { FormsModule } from '@angular/forms';
import { Ex1Component } from './ex1/ex1.component';
import { Demo2Component } from './demo2/demo2.component';
import { HttpClientModule } from '@angular/common/http';
import { ToCelciusPipe } from './pipes/to-celcius.pipe';
import { ToUrlPipe } from './pipes/to-url.pipe';
import { ChartsModule } from 'ng2-charts';
import { PokedexComponent } from './pokedex/pokedex.component';
import { DetailsComponent } from './pokedex/details/details.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    Demo1Component,
    Ex1Component,
    Demo2Component,
    ToCelciusPipe,
    ToUrlPipe,
    PokedexComponent,
    DetailsComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default' }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbSidebarModule.forRoot(),
    NbButtonModule,
    NbInputModule,
    NbMenuModule.forRoot(),
    //nécessaire pour le Binding 2 Ways
    FormsModule,
    NbIconModule,
    NbCardModule,
    NbListModule,
    // MODULE PERMETTANT D'UTILISER LE SERVICE HTTPCLIENT
    HttpClientModule,
    // module de nebular pour afficher des toasts
    NbToastrModule.forRoot(),
    // module pour l'utilisation des charts dans angular
    ChartsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
