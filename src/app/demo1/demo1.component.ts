import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {


  isHidden: boolean = false;

  liste: string[] = ["sel", "poivre", "sucre"];

  maVariable: string = "Khun";

  constructor() { }

  ngOnInit(): void {
  }

  maFonction() {
    this.isHidden = !this.isHidden;
  }

}
