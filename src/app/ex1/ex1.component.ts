import { Component, OnInit } from '@angular/core';
import { Article } from '../models/article';

@Component({
  selector: 'app-ex1',
  templateUrl: './ex1.component.html',
  styleUrls: ['./ex1.component.scss']
})
export class Ex1Component implements OnInit {

  newItem: string;
  shoppingList: Article[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  add() {
    //let index = this.shoppingList.indexOf();
    let article = this.shoppingList.find(a => a.title == this.newItem);
    //let index = this.shoppingList.indexOf(article);
    if(article != null) {
      // existe déjà
      // modifier la quantité de l'article
      article.quantity++;
    }
    else{
      // n'existe pas
      this.shoppingList.push({
        title: this.newItem,
        isChecked: false,
        quantity: 1
      });
    }
    this.newItem = null;
  }

  // remove(item: string) {
  //   let index = this.shoppingList.indexOf(item);
  //   this.shoppingList.splice(index, 1);
  // }

  remove(index: number) {
    this.shoppingList.splice(index, 1);
  }

  check(item: Article) {
    item.isChecked = !item.isChecked;
  }
}

/*
JSON JS Object Notation

example
{
  title: valeur,
  price: valeur
}

*/
