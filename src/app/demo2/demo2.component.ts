import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MeteoInfo } from '../models/meteo-info';
import { SuggestionsInfo } from '../models/suggestions-info';
import { NbToastrService } from '@nebular/theme';
import { ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { ForeCast } from '../models/fore-cast';

@Component({
  selector: 'app-demo2',
  templateUrl: './demo2.component.html',
  styleUrls: ['./demo2.component.scss']
})
export class Demo2Component implements OnInit {

  baseURL: string = "https://api.openweathermap.org/data/2.5/weather?q={city},{countryCode}&appid={apiKey}";
  baseForcastURL: string = "https://api.openweathermap.org/data/2.5/forecast/daily?q={city},{countryCode}&appid={apiKey}&units=metric";
  baseGeocodeURL: string = "https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=iw8TUjRrGnKBfvRXiSEzQNZ_2__ThpSJc2wHWMA1VPM&query={keyword}";
  apiKey: string = "d52e50e34214ff0b92247f788638eeb9";

  model: MeteoInfo;

  model2: SuggestionsInfo;

  cityName: string = '';

  dataSets: ChartDataSets[] = [
  ];

  labels: Label[] = [];

  constructor(
    private client: HttpClient,
    private toastr: NbToastrService
  ) { }

  ngOnInit(): void {
    //this.getMeteo();
  }

  getSuggestions() {
    console.log(44)
    if(this.cityName.length >= 4) {
      this.client.get<any>(this.baseGeocodeURL.replace('{keyword}', this.cityName))
        .subscribe(json => {
          console.log(json);
          
          this.model2 = json;
        });
    }
  }

  getMeteo() {
    let sugg = this.model2.suggestions.find(s => s.label == this.cityName);
    this.client.get<MeteoInfo>(
      this.baseURL
        .replace("{city}", encodeURI(sugg.address[sugg.matchLevel]))
        .replace("{state}", encodeURI(sugg.address.state))
        .replace("{countryCode}", sugg.countryCode.substr(0,2))
        .replace("{apiKey}", this.apiKey)
    ).subscribe(
      // excécuter en cas de success 
      json => {
      // attendre la réponse de openweathermap
        console.log(43);
        this.model = json;
      },
      //excécuter en cas d'erreur 
      error => {
        // afficher un message d'erreur
        this.toastr.danger("Impossible de trouver les infos meteos de ville encodée")
      }, /* Dans tous les cas */() => {});
    // ne pas attendre la réponse
    console.log(42);
  } 

  getForeCast() {
    let sugg = this.model2.suggestions.find(s => s.label == this.cityName);
    this.client.get<ForeCast>(
      this.baseForcastURL
        .replace("{city}", encodeURI(sugg.address[sugg.matchLevel]))
        .replace("{state}", encodeURI(sugg.address.state))
        .replace("{countryCode}", sugg.countryCode.substr(0,2))
        .replace("{apiKey}", this.apiKey)
    ).subscribe(
      // excécuter en cas de success 
      json => {
      // attendre la réponse de openweathermap
        console.log(json);
        this.labels = json.list.map(data => {
          return new Date(data.dt * 1000).toDateString();
        });
        this.dataSets.push({
          label: json.city.name,
          data: json.list.map(d => d.temp.max)
        })
      },
      //excécuter en cas d'erreur 
      error => {
        // afficher un message d'erreur
        this.toastr.danger("Impossible de trouver les infos meteos de ville encodée")
      }, /* Dans tous les cas */() => {});
    // ne pas attendre la réponse
    console.log(42);
  }
}
