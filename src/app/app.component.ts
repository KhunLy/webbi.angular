import { Component } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  links: NbMenuItem[] = [
    { link: "/home", title: "Home Page", icon: "home" },
    { link: "/about", title: "A Propos", icon: "book" },
    { link: "/demo1", title: "Demo 1 - *ngIf", icon: "color-palette" },
    { link: "/ex1", title: "Ex 1 - Shopping List", icon: "shopping-cart" },
    { link: "/meteo", title: "DEMO 2 - METEO", icon: "sun" },
    { link: "/pokedex", title: "Pokedex", icon: "github" }
  ]
}
