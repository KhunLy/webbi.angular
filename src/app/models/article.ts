export interface Article {
    title: string;
    isChecked: boolean;
    quantity: number;
}